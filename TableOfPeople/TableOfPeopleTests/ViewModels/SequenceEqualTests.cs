﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TableOfPeople.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace TableOfPeople.ViewModels.Tests
{
    [TestClass()]
    public class SequenceEqualTests
    {
        private PersonViewModel GeneratePerson_1()
        {
            return new PersonViewModel
            {
                ApartmentNumber = "10",
                BirthDate = new DateTime(2010, 10, 10),
                FirstName = "Test_1",
                LastName = "Test_1",
                HouseNumber = "10",
                PhoneNumber = "1234",
                PostalCode = "22222",
                StreetName = "Test",
                Town = null
            };
        }

        private PersonViewModel GeneratePerson_2()
        {
            return new PersonViewModel
            {
                ApartmentNumber = "10",
                BirthDate = new DateTime(2010, 10, 9),
                FirstName = "Test_1",
                LastName = "Test_1",
                HouseNumber = "10",
                PhoneNumber = "1234",
                PostalCode = "22222",
                StreetName = "Test",
                Town = null
            };
        }

        private PersonViewModel GenerateNullPerson()
        {
            return null;
        }

        [TestMethod()]
        public void GetHashCodeSameObjectByValueTest()
        {
            PersonViewModel person1 = GeneratePerson_1();
            PersonViewModel person2 = GeneratePerson_1();
            Assert.IsTrue(person1.GetHashCode() == person2.GetHashCode());
        }

        [TestMethod()]
        public void GetHashCodeDiffrentObjectByValueTest()
        {
            PersonViewModel person1 = GeneratePerson_1();
            PersonViewModel person2 = GeneratePerson_2();
            Assert.IsFalse(person1.GetHashCode() == person2.GetHashCode());
        }


        [TestMethod()]
        public void IsEqualSameObjectByValueTest()
        {
            PersonViewModel person1 = GeneratePerson_1();
            PersonViewModel person2 = GeneratePerson_1();
            Assert.IsTrue(person1.Equals(person2));
        }


        [TestMethod()]
        public void IsEqualDiffrentObjectByValueTest()
        {
            PersonViewModel person1 = GeneratePerson_1();
            PersonViewModel person2 = GeneratePerson_2();
            Assert.IsFalse(person1.Equals(person2));
        }


        [TestMethod()]
        public void IsEqualNullTest()
        {
            PersonViewModel person1 = GeneratePerson_1();
            PersonViewModel person2 = GenerateNullPerson();
            Assert.IsFalse(person1.Equals(person2));
        }

        [TestMethod()]
        public void SequenceEqualEmptyLists()
        {
            ObservableCollection<PersonViewModel> list1 = new ObservableCollection<PersonViewModel>
            {
            };
            ObservableCollection<PersonViewModel> list2 = new ObservableCollection<PersonViewModel>
            {
            };
            Assert.IsTrue(list1.SequenceEqual(list2));
        }


        [TestMethod()]
        public void SequenceEqualEqualLists()
        {
            ObservableCollection<PersonViewModel> list1 = new ObservableCollection<PersonViewModel>
            {
                GeneratePerson_1(),
                GeneratePerson_2()
            };
            ObservableCollection<PersonViewModel> list2 = new ObservableCollection<PersonViewModel>
            {
                GeneratePerson_1(),
                GeneratePerson_2()
            };
            Assert.IsTrue(list1.SequenceEqual(list2));
        }

        [TestMethod()]
        public void SequenceEqualNotEqualLists()
        {
            ObservableCollection<PersonViewModel> list1 = new ObservableCollection<PersonViewModel>
            {
                GeneratePerson_1(),
                GeneratePerson_2()
            };
            ObservableCollection<PersonViewModel> list2 = new ObservableCollection<PersonViewModel>
            {
                GeneratePerson_1(),
                GeneratePerson_1()
            };
            Assert.IsFalse(list1.SequenceEqual(list2));
        }

        [TestMethod()]
        public void SequenceEqualListsWithNulls()
        {
            ObservableCollection<PersonViewModel> list1 = new ObservableCollection<PersonViewModel>
            {
                GenerateNullPerson(),
            };
            ObservableCollection<PersonViewModel> list2 = new ObservableCollection<PersonViewModel>
            {
                GenerateNullPerson(),
            };
            Assert.IsTrue(list1.SequenceEqual(list2));
        }
    }
}