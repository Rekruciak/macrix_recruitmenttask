﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using TableOfPeople.Views;
using WPF_ThirdParty.Windows;

namespace TableOfPeople
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            BaseWindowWithChangableContent window = new BasicWindow("Table of people");
            window.Width = 1024;
            window.Height = 600;
            window.WindowStartupLocation = WindowStartupLocation.CenterScreen;

            WPF_ThirdParty.OnStartup.StartApplication(new TableOfPeopleView(), window);
        }
    }
}
