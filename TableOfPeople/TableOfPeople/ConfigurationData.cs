﻿using ITA.IO_ThirdParty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableOfPeople
{
    public class ConfigurationData
    {
        public static string SerializationDirectoryPath
        {
            get => System.IO.Path.Combine(Paths.Documents, "Table of people");
        }

        public static string SerializationFilePath
        {
            get => System.IO.Path.Combine(SerializationDirectoryPath, "Serialized data.xml");
        }
    }
}
