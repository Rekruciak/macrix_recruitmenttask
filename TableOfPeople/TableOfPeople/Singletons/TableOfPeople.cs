﻿using ITA.IO_ThirdParty;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TableOfPeople.ViewModels;

namespace TableOfPeople.Singletons
{
    public class TableOfPeople
    {
        private static TableOfPeople _instance;
        
        /// <summary>
        /// Ctor for serialization, use Instance->get instead.
        /// </summary>
        public TableOfPeople() { }

        #region Initialization with deserialization
        private static bool DeserializePassed (out TableOfPeople tableOfPeople)
        {
            tableOfPeople = null;
            try
            {
                tableOfPeople = Serialization.Deserialize<TableOfPeople>(
                    DirectoriesFilesOperations.GetFileContent(ConfigurationData.SerializationFilePath));
                return true;
            }
            catch(Exception ex)
            {
                Console.WriteLine($@"Exception on deserialization

{ex.ToString()}");
                return false;
            }
        }

        private static TableOfPeople InitializeInstance()
        {
            DirectoriesFilesOperations.CreateDirectoryIfDoesntExists(
                ConfigurationData.SerializationDirectoryPath);

            return DirectoriesFilesOperations.DoesFileExists(ConfigurationData.SerializationFilePath) &&
                DeserializePassed(out TableOfPeople tableOfPeople)
                ? tableOfPeople
                : new TableOfPeople();
        }
        #endregion

        public static TableOfPeople Instance
        {
            get 
            {
                if (_instance == null)
                    _instance = InitializeInstance();
                return _instance;
            }
        }

        public ObservableCollection<PersonViewModel> People { get; set; }

        public void Serialize()
        {
            DirectoriesFilesOperations.MakeFileWithSpecificContent(
                ConfigurationData.SerializationFilePath,
                Serialization.Serialize(this));
        }
    }
}
