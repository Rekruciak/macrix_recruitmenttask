﻿using ITA.IO_ThirdParty;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WPF_ThirdParty;
using WPF_ThirdParty.ShowDialogs;
using WPF_ThirdParty.VM_Implementation;

namespace TableOfPeople.ViewModels
{
    public class TableOfPeopleViewModel : BaseViewModel
    {
        private ObservableCollection<PersonViewModel> _peopleBackUp;
        public TableOfPeopleViewModel()
        {
            People = Singletons.TableOfPeople.Instance.People;
            _peopleBackUp = Serialization.CloneObjectByValueWithSerialization(People);
        }

        private ObservableCollection<PersonViewModel> _people;
        public ObservableCollection<PersonViewModel> People
        {
            get => _people;
            set
            {
                _people = value;
                OnPropertyChanged(nameof(People));
            }
        }

        public ICommand AddNewPersonCommand
        {
            get => new RelayCommand(() =>
            {
                PersonViewModel newPerson = new PersonViewModel();
                People.Add(newPerson);
            });
        }

        public ICommand RemovePersonCommand
        {
            get => new RelayCommand<PersonViewModel>(delegate (PersonViewModel personToRemove)
            {
                if (personToRemove == null)
                    return;

                if (personToRemove.IsEmpty ||
                    MessageDialog.AskQuestion(
$@"Removing {personToRemove.FirstName} {personToRemove.LastName} from table", "Confirm", "Cancel"))
                    People.Remove(personToRemove);
            });
        }

        public bool AreUnsavedChanges()
        {
            return !_peopleBackUp.SequenceEqual(People);
        }

        public ICommand SaveCommand
        {
            get => new RelayCommand(() =>
            {
                Singletons.TableOfPeople.Instance.Serialize();
                _peopleBackUp = Serialization.CloneObjectByValueWithSerialization(People);
            }, AreUnsavedChanges);
        }

        public ICommand CancelCommand
        {
            get => new RelayCommand(() =>
            {
                if (MessageDialog.AskQuestion($@"Discard all unsaved changes?", "Yes", "No"))
                    People = Serialization.CloneObjectByValueWithSerialization(_peopleBackUp);
            }, AreUnsavedChanges);
        }
    }
}
