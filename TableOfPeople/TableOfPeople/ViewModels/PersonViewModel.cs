﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WPF_ThirdParty;

namespace TableOfPeople.ViewModels
{
    public class PersonViewModel : BaseViewModel
    {
        private Person _person { get; set; }
        public PersonViewModel() : this(new Person())
        { }
        public PersonViewModel(Person person)
        {
            _person = person;
        }

        #region Binded data
        public string FirstName
        {
            get => _person.FirstName;
            set
            {
                if ((value ?? "").Any(q => char.IsDigit(q)))
                    return;
                _person.FirstName = value;
                OnPropertyChanged(nameof(IsValid));
                OnPropertyChanged(nameof(IsEmpty));
            }
        }
        public string LastName
        {
            get => _person.LastName;
            set
            {
                if ((value ?? "").Any(q => char.IsDigit(q)))
                    return;
                _person.LastName = value;
                OnPropertyChanged(nameof(IsValid));
                OnPropertyChanged(nameof(IsEmpty));
            }
        }
        public string StreetName
        {
            get => _person.StreetName;
            set
            {
                if ((value ?? "").Any(q => char.IsDigit(q)))
                    return;
                _person.StreetName = value;
                OnPropertyChanged(nameof(IsValid));
                OnPropertyChanged(nameof(IsEmpty));
            }
        }
        public string HouseNumber
        {
            get => _person.HouseNumber;
            set
            {
                _person.HouseNumber = value;
                OnPropertyChanged(nameof(IsValid));
                OnPropertyChanged(nameof(IsEmpty));
            }
        }
        public string ApartmentNumber
        {
            get => _person.ApartmentNumber;
            set
            {
                _person.ApartmentNumber = value;
                OnPropertyChanged(nameof(IsValid));
                OnPropertyChanged(nameof(IsEmpty));
            }
        }
        public string PostalCode
        {
            get => _person.PostalCode;
            set
            {
                _person.PostalCode = value;
                OnPropertyChanged(nameof(IsValid));
                OnPropertyChanged(nameof(IsEmpty));
            }
        }
        public string Town
        {
            get => _person.Town;
            set
            {
                if ((value ?? "").Any(q => char.IsDigit(q)))
                    return;
                _person.Town = value;
                OnPropertyChanged(nameof(IsValid));
                OnPropertyChanged(nameof(IsEmpty));
            }
        }
        public string PhoneNumber
        {
            get => _person.PhoneNumber;
            set
            {
                if ((value ?? "").Any(q => char.IsLetter(q)))
                    return;
                _person.PhoneNumber = value;
                OnPropertyChanged(nameof(IsValid));
                OnPropertyChanged(nameof(IsEmpty));
            }
        }
        public DateTime? BirthDate
        {
            get => _person.BirthDate;
            set
            {
                if (value < DateTime.Now && 
                    value > new DateTime(1900,1,1))
                {
                    _person.BirthDate = value;
                    OnPropertyChanged(nameof(Age));
                    OnPropertyChanged(nameof(IsValid));
                    OnPropertyChanged(nameof(IsEmpty));
                }
            }
        }
        public int Age => BirthDate != null
            ? DateTime.Now.Year - ((DateTime)BirthDate).Year +
               (((DateTime.Now.Month == ((DateTime)BirthDate).Month && DateTime.Now.Month == ((DateTime)BirthDate).Month) ||
                DateTime.Now.Month == ((DateTime)BirthDate).Month)
                    ? 1
                    : 0)
            : 0;

        public bool IsValid => CheckIsValid();
        public string NotValidMessage { get; set; }

        #endregion

        public bool CheckIsValid()
        {
            StringBuilder notValidStringBuilder = new StringBuilder();

            if (!IsEmpty)
            {
                if (String.IsNullOrEmpty(FirstName))
                    notValidStringBuilder.AppendLine($@"First name is required");
                if ((FirstName ?? "").Any(q => Char.IsDigit(q)))
                    notValidStringBuilder.AppendLine($@"First name shouldn't contain digits");

                if (String.IsNullOrEmpty(LastName))
                    notValidStringBuilder.AppendLine($@"Last name is required");
                if ((LastName ?? "").Any(q => Char.IsDigit(q)))
                    notValidStringBuilder.AppendLine($@"Last name shouldn't contain digits");

                if (String.IsNullOrEmpty(StreetName))
                    notValidStringBuilder.AppendLine($@"Street name is required");

                if (String.IsNullOrEmpty(HouseNumber))
                    notValidStringBuilder.AppendLine($@"House number is required");
                if (!(HouseNumber ?? "").Any(q => Char.IsDigit(q)))
                    notValidStringBuilder.AppendLine($@"House number should contain some digits");

                if (String.IsNullOrEmpty(PostalCode))
                    notValidStringBuilder.AppendLine($@"Postal code is required");

                if (String.IsNullOrEmpty(Town))
                    notValidStringBuilder.AppendLine($@"Town is required");

                if (String.IsNullOrEmpty(PhoneNumber))
                    notValidStringBuilder.AppendLine($@"Phone number is required");
                else if (!(new Regex("^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\s\\./0-9]*$")).IsMatch(PhoneNumber))
                    notValidStringBuilder.AppendLine($@"Phone number format is not ok");

                if (!BirthDate.HasValue)
                    notValidStringBuilder.AppendLine($@"Birth date is required");
            }


            NotValidMessage = notValidStringBuilder.ToString();
            OnPropertyChanged(nameof(NotValidMessage));
            return String.IsNullOrEmpty(NotValidMessage);
        }

        private IEnumerable<PropertyInfo> AllPersonProperties => typeof(Person).GetProperties();

        private object GetPropertyDefaultValue(PropertyInfo propertyInfo)
        {
            return propertyInfo.PropertyType.IsValueType
                ? Activator.CreateInstance(propertyInfo.PropertyType)
                : null;
        }

        private bool ArePropertyValuesEqual(object value1, object value2)
        {
            return (value1 ?? "").Equals(value2 ?? "");
        }

        public bool IsEmpty 
        {
            get => AllPersonProperties.All(
                q => ArePropertyValuesEqual(q.GetValue(this._person ), GetPropertyDefaultValue(q)));
        }

        #region Sequence equal comparers
        public override bool Equals(object obj)
        {
            if (!(obj is PersonViewModel item))
                return false;

            return AllPersonProperties.All(q =>
                 ArePropertyValuesEqual(q.GetValue(this._person), q.GetValue(item._person)));
        }

        public override int GetHashCode()
        {
            var hashCode = 1840996834;
            foreach (var valueTypePropertyInfo in AllPersonProperties.Where(q => q.GetType().IsValueType))
                hashCode = hashCode * -1521134295 + valueTypePropertyInfo.GetValue(this._person).GetHashCode();
            foreach (var referenceTypePropertyInfo in AllPersonProperties.Where(q => !q.GetType().IsValueType && q.GetValue(this._person) != null))
                hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(referenceTypePropertyInfo.GetValue(this._person).ToString());
            return hashCode;
        }
        #endregion
    }
}
