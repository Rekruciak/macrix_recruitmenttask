﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TableOfPeople.ViewModels;
using WPF_ThirdParty.ExtensionMethods;

namespace TableOfPeople.Views
{
    /// <summary>
    /// Interaction logic for TableOfPeopleView.xaml
    /// </summary>
    public partial class TableOfPeopleView : UserControl
    {
        public TableOfPeopleView()
        {
            InitializeComponent();
            this.DataContext = new TableOfPeopleViewModel();
        }

        private void TextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (sender is TextBox textbox)
                textbox.SelectAllText();
        }
    }
}
