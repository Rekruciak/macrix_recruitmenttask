﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using WPF_ThirdParty.Windows;

namespace WPF_ThirdParty
{
    public class ScreenController
    {
        private readonly BaseWindowWithChangableContent _window;
        private static ScreenController _screenController;

        public ScreenController(BaseWindowWithChangableContent window)
        {
            _window = window;
            window.Closed += Window_Closed;
            _screenController = this;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }

        public static ScreenController Instance
        {
            get => _screenController;
        }

        public BaseWindowWithChangableContent Window
        {
            get => _window;
        }

        public void SetWindowContent(UserControl userControl)
        {
            _window.SetWindowContent(userControl);
        }

        public void CloseWindow()
        {
            _window.Close();
        }

        public void ShowWindow()
        {
            _window.Show();
        }

        public void ShowDialog()
        {
            _window.ShowDialog();
        }
    }
}
