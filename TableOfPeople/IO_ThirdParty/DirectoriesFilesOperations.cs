﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITA.IO_ThirdParty
{
    public class DirectoriesFilesOperations
    {
        public static void CreateDirectoryIfDoesntExists(string fullPathToDirectory)
        {
            Directory.CreateDirectory(fullPathToDirectory);
        }

        public static bool DoesFileExists(string fullPathToFile)
        {
            return File.Exists(fullPathToFile);
        }

        public static string GetFileContent(string fullPathToFile)
        {
            return File.ReadAllText(fullPathToFile);
        }

        public static void MakeFileWithSpecificContent(string fullPathToFile, string content)
        {
            File.WriteAllText(fullPathToFile, content);
        }
    }
}
